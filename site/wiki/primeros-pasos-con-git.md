```{post} 2023-06-30
:author: GuayaHack
:tags: newbie, git
:category: wiki
:language: Español
:location: Heidelberg, Germany
:excerpt: 1
```

# Primeros Pasos con Git

Hola chic@s, como ya saben siempre hay que buscar una forma de organizarse para que cada quien vaya aprendiendo a su ritmo. Una de las herramientas más importantes para cualquier programador es su propio proceso: pensar, escribir, probar, ejecutar, observar, y volver al primer paso.

Git es una herramienta de versionamiento que nos ayuda a materializar ese proceso y no dañar el código ni modificarlo en formas inesperadas y saber siempre y en todo momento quien, como y cuando realizo cambios. Existen plataformas como GitHub y GitLab que permiten gestionar proyectos y comunidades enteras en lo que se llama grupos, projectos y repositorios.

Nosotros usamos el grupo de GuayaHack en https://gitlab.com/guayahack para todos los proyectos y el repositorio `guayahack/main` para la página y ésta wiki https://gitlab.com/guayahack/main.

Éste es el primer paso que darán hacia poquito a poquito ir aprendiendo a defenderse con las herramientas de un programador y el *proceso* de la creación de software. Por eso, la primera tarea es agregarse como participantes de GuayaHack en la página de la comunidad {doc}`/community/index`


## GitLab

### Grupos

### Proyectos

### Repositorios

## Git

### Instalando Git

### El Ciclo Normal de Git

### Contribuyendo a un Repositorio

#### Fork

#### Clone

#### Branch

#### Push

#### Merge

#### Verificación de Cambios


## Contribuciones 

@jdsalaro
