
# Comunidad

```{toctree}
:maxdepth: 1
:hidden:
:glob:
./*
```


## Miembros

Aunque GuayaHack es una iniciativa creada por {doc}`/community/member/jdsalaro`, es un esfuerzo colaborativo de tod@s para tod@s, éstos son sus miembros:

## Voluntarios

### Moderadores

Todos los moderadores son tutores.

1. {doc}`/community/member/jdsalaro`
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Tutores

Todos los tutores son participantes.

1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Participantes

1. {doc}`/community/member/danteboe`
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
